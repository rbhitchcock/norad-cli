# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'norad_cli/version'

Gem::Specification.new do |spec|
  spec.name          = 'norad_cli'
  spec.version       = NoradCli::VERSION
  spec.authors       = ['Blake Hitchcock', 'Brian Manifold', 'Roger Seagle']
  spec.email         = ['rbhitchcock@gmail.com', 'bmanifold@gmail.com ', 'roger.seagle@gmail.com']

  spec.summary       = 'Command line interface for norad.'
  spec.description   = 'Command line interface for norad.'
  spec.homepage      = 'https://gitlab.com/norad/cli'
  spec.license       = 'Apache-2.0'
  spec.required_ruby_version = '>= 2.2.0'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Regular Dependencies
  spec.add_dependency 'docker-api'
  spec.add_dependency 'git'
  spec.add_dependency 'rainbow', '~> 2.2.1'
  spec.add_dependency 'rspec', '~> 3.0'
  spec.add_dependency 'safe_yaml'
  spec.add_dependency 'thor'

  # Development Dependencies
  spec.add_development_dependency 'bundler', '~> 1.12'
  spec.add_development_dependency 'bundler-audit', '~> 0.5'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.47'
end
