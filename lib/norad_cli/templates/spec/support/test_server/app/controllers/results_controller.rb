class ResultsController < ApplicationController
  # GET /results
  def index
    @results = Result.where(assessment_id: params[:assessment_id])
    render json: @results
  end

  # POST /results
  def create
    params[:results].each do |result|
      Result.create(
        assessment_id: params[:id],
        nid: result[:nid],
	sir: result[:sir],
	status: result[:status],
	output: result[:output],
	title: result[:title],
	description: result[:description]
      )
    end
    render json: Result.where(assessment_id: params[:id]), status: :created
  end
end
