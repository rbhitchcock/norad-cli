Rails.application.routes.draw do
  resources :results, only: :index
  post 'results/:id', to: 'results#create', id: /\w{64}/
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
