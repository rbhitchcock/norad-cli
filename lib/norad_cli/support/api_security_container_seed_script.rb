# frozen_string_literal: true

require 'yaml'

module SeedGenerator
  def self.add_container(container_file, m, long_name, docsite, readme_anchor)
    container_file.write("\n") unless container_file.size.zero?
    container_file.write("sc = SecurityContainer.where(name: '#{long_name}').first_or_initialize\n")
    container_file.write("sc.prog_args = '#{m['prog_args']}'\n")
    container_file.write("sc.default_config = #{symbolized_config(m['default_config'])}\n") if m.key?('default_config')
    container_file.write("sc.category = :#{m['category'].to_sym}\n")
    container_file.write("sc.test_types = #{m['test_types']}\n")
    container_file.write("sc.common_service_type = CommonServiceType.find_by(name: '#{m['common_service_type']}')\n") if m.key?('common_service_type')
    container_file.write("sc.configurable = #{m['configurable']}\n") if m.key?('configurable')
    container_file.write("sc.multi_host = #{m['multi_host']}\n") if m.key?('multi_host')
    container_file.write("sc.help_url = '#{docsite}##{readme_anchor}'\n")
    container_file.write("sc.save!\n")
  end

  def self.symbolized_config(config)
    '{ ' + config.reduce([]) do |a, p|
      a << "#{p.first}: '#{p.last}'"
    end.join(', ') + ' }'
  end

  def self.readme_anchor(readme)
    title = readme.readline
    # ## My Test Name123
    # Trim off ## portion of title, replace spaces with -, and downcase
    title.match(/\s*##\s+(.+)/)[1].strip.gsub(/[ .]/, '-').downcase
  end

  def self.process_manifests(seed_file, docsite)
    File.open(seed_file, 'w') do |container_file|
      Dir.glob('./**/manifest.yml').each do |p|
        manifest = YAML.load(File.new(p, 'r').read)
        readme_anchor = readme_anchor(File.new(p.gsub(/manifest\.yml/, 'README.md')))
        long_name = "#{manifest['registry']}/#{manifest['name']}:#{manifest['version']}"
        add_container(container_file, manifest, long_name, docsite, readme_anchor)
      end
    end
  end
end
