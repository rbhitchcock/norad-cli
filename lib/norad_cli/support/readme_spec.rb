# frozen_string_literal: true

class Readme
end

describe Readme do
  def testing_for(s)
    puts "Testing for #{s}..."
  end

  def ok
    puts 'OK'
  end

  it 'requires each tool to have a README' do
    next if File.exist?("sectests/#{ENV['sectest_name']}/.parent_only")
    puts "Looking for README.md in: sectests/#{ENV['sectest_name']}..."
    readme = File.read "sectests/#{ENV['sectest_name']}/README.md"
    expect(readme.lines[0].tr(' ', '-')).to match(/#{ENV['sectest_name']}/i)
    ok

    testing_for 'variant READMEs'
    puts 'No variants for this tool' unless Dir.exist?("sectests/#{ENV['sectest_name']}/variants")
    ok
    Dir.glob("sectests/#{ENV['sectest_name']}/variants/*").each do |variant|
      readme = File.read "#{variant}/README.md"
      ok
    end
  end
end
